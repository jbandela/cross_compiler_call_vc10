
namespace cross_compiler_interface{

	namespace detail {
		template<class F>
		struct fn_ptr_helper{};

		// 5 Parms
		template<class R, class Parms,class Parms2,class Parms3,class Parms4,class Parms5>
		struct fn_ptr_helper<R(Parms,Parms2,Parms3,Parms4,Parms5)>{
			typedef R (CROSS_CALL_CALLING_CONVENTION *fn_ptr_t)(Parms,Parms2,Parms3,Parms4,Parms5);

		};
		// 4 Parms
		template<class R, class Parms,class Parms2,class Parms3,class Parms4>
		struct fn_ptr_helper<R(Parms,Parms2,Parms3,Parms4)>{
			typedef R (CROSS_CALL_CALLING_CONVENTION *fn_ptr_t)(Parms,Parms2,Parms3,Parms4);

		};
		// 3 Parms
		template<class R, class Parms,class Parms2,class Parms3>
		struct fn_ptr_helper<R(Parms,Parms2,Parms3)>{
			typedef R (CROSS_CALL_CALLING_CONVENTION *fn_ptr_t)(Parms,Parms2,Parms3);

		};
		// 2 Parms
		template<class R, class Parms,class Parms2>
		struct fn_ptr_helper<R(Parms,Parms2)>{
			typedef R (CROSS_CALL_CALLING_CONVENTION *fn_ptr_t)(Parms,Parms2);

		};
		// 1 Parms
		template<class R, class Parms>
		struct fn_ptr_helper<R(Parms)>{
			typedef R (CROSS_CALL_CALLING_CONVENTION *fn_ptr_t)(Parms);

		};
		// 0 Parms
		template<class R>
		struct fn_ptr_helper<R()>{
			typedef R (CROSS_CALL_CALLING_CONVENTION *fn_ptr_t)();

		};


		template<template<class> class Iface, class Derived,int N, class FuncType, class F>
		struct custom_function_vtable_functions{

			typedef typename fn_ptr_helper<F>::fn_ptr_t vtable_fn_ptr_t;

			struct helper{
				portable_base* p_;
				helper(portable_base* p):p_(p){}

				FuncType& get_function(){
					return detail::get_function<N,FuncType>(p_);
				}

				template<class C>
				C* get_mem_fn_object(){

					return detail::get_data<N,C>(p_);
				}

				error_code error_code_from_exception(std::exception& e){
					return error_mapper<Iface>::mapper::error_code_from_exception(e);
				}
				// Support up to 5 parameters
				template<class Parms, class Parms2,class Parms3, class Parms4, class Parms5>
				error_code forward_to_runtime_parent(Parms p,Parms2 p2, Parms3 p3, Parms4 p4, Parms5 p5){
					// See if runtime inheritance present with parent
					vtable_n_base* vn = static_cast<vtable_n_base*>(p_);
					if(vn->runtime_parent_){
						// call the parent
						return reinterpret_cast<vtable_fn_ptr_t>(vn->runtime_parent_->vfptr[N])(vn->runtime_parent_,p,p2,p3,p4,p5);
					}
					else{
						return error_not_implemented::ec;
					}
				}
				// Support  to 4 parameters
				template<class Parms, class Parms2,class Parms3, class Parms4>
				error_code forward_to_runtime_parent(Parms p,Parms2 p2, Parms3 p3, Parms4 p4){
					// See if runtime inheritance present with parent
					vtable_n_base* vn = static_cast<vtable_n_base*>(p_);
					if(vn->runtime_parent_){
						// call the parent
						return reinterpret_cast<vtable_fn_ptr_t>(vn->runtime_parent_->vfptr[N])(vn->runtime_parent_,p,p2,p3,p4);
					}
					else{
						return error_not_implemented::ec;
					}
				}
				// Support  to 3 parameters
				template<class Parms, class Parms2,class Parms3>
				error_code forward_to_runtime_parent(Parms p,Parms2 p2, Parms3 p3){
					// See if runtime inheritance present with parent
					vtable_n_base* vn = static_cast<vtable_n_base*>(p_);
					if(vn->runtime_parent_){
						// call the parent
						return reinterpret_cast<vtable_fn_ptr_t>(vn->runtime_parent_->vfptr[N])(vn->runtime_parent_,p,p2,p3);
					}
					else{
						return error_not_implemented::ec;
					}
				}
				// Support  to 2 parameters
				template<class Parms, class Parms2>
				error_code forward_to_runtime_parent(Parms p,Parms2 p2){
					// See if runtime inheritance present with parent
					vtable_n_base* vn = static_cast<vtable_n_base*>(p_);
					if(vn->runtime_parent_){
						// call the parent
						return reinterpret_cast<vtable_fn_ptr_t>(vn->runtime_parent_->vfptr[N])(vn->runtime_parent_,p,p2);
					}
					else{
						return error_not_implemented::ec;
					}
				}
				// Support  to 1 parameters
				template<class Parms>
				error_code forward_to_runtime_parent(Parms p){
					// See if runtime inheritance present with parent
					vtable_n_base* vn = static_cast<vtable_n_base*>(p_);
					if(vn->runtime_parent_){
						// call the parent
						return reinterpret_cast<vtable_fn_ptr_t>(vn->runtime_parent_->vfptr[N])(vn->runtime_parent_,p);
					}
					else{
						return error_not_implemented::ec;
					}
				}
				// Support  to 0 parameters
				error_code forward_to_runtime_parent(){
					// See if runtime inheritance present with parent
					vtable_n_base* vn = static_cast<vtable_n_base*>(p_);
					if(vn->runtime_parent_){
						// call the parent
						return reinterpret_cast<vtable_fn_ptr_t>(vn->runtime_parent_->vfptr[N])(vn->runtime_parent_);
					}
					else{
						return error_not_implemented::ec;
					}
				}


			};
			template<class F_R_Parms>
			struct vte{};

			// 5 parameters
			template<class R,class Parms, class Parms2,class Parms3, class Parms4, class Parms5>
			struct vte<R(Parms,Parms2,Parms3,Parms4,Parms5)>{
			static R CROSS_CALL_CALLING_CONVENTION vtable_entry_function(Parms p,Parms2 p2, Parms3 p3, Parms4 p4, Parms5 p5){
				helper h(p);
				try{
					auto f = h.get_function();
					if(!f){
						return h.forward_to_runtime_parent(p2,p3,p4,p5);
					}
					return Derived::vtable_function(f,p,p2,p3,p4,p5);
				} catch(std::exception& e){
					return h.error_code_from_exception(e);
				}

			}		
			};
			// 4 parameters
			template<class R,class Parms, class Parms2,class Parms3, class Parms4>
			struct vte<R(Parms,Parms2,Parms3,Parms4)>{
			static R CROSS_CALL_CALLING_CONVENTION vtable_entry_function(Parms p,Parms2 p2, Parms3 p3, Parms4 p4){
				helper h(p);
				try{
					auto f = h.get_function();
					if(!f){
						return h.forward_to_runtime_parent(p2,p3,p4);
					}
					return Derived::vtable_function(f,p,p2,p3,p4);
				} catch(std::exception& e){
					return h.error_code_from_exception(e);
				}

			}		
			};
			// 3 parameters
			template<class R,class Parms, class Parms2,class Parms3>
			struct vte<R(Parms,Parms2,Parms3)>{
			static R CROSS_CALL_CALLING_CONVENTION vtable_entry_function(Parms p,Parms2 p2, Parms3 p3){
				helper h(p);
				try{
					auto f = h.get_function();
					if(!f){
						return h.forward_to_runtime_parent(p2,p3);
					}
					return Derived::vtable_function(f,p,p2,p3);
				} catch(std::exception& e){
					return h.error_code_from_exception(e);
				}

			}		
			};
			// 2 parameters
			template<class R,class Parms, class Parms2>
			struct vte<R(Parms,Parms2)>{
			static R CROSS_CALL_CALLING_CONVENTION vtable_entry_function(Parms p,Parms2 p2){
				helper h(p);
				try{
					auto f = h.get_function();
					if(!f){
						return h.forward_to_runtime_parent(p2);
					}
					return Derived::vtable_function(f,p,p2);
				} catch(std::exception& e){
					return h.error_code_from_exception(e);
				}

			}		
			};
			// 1 parameters
			template<class R,class Parms>
			struct vte<R(Parms)>{
			static R CROSS_CALL_CALLING_CONVENTION vtable_entry_function(Parms p){
				helper h(p);
				try{
					auto f = h.get_function();
					if(!f){
						return h.forward_to_runtime_parent();
					}
					return Derived::vtable_function(f,p);
				} catch(std::exception& e){
					return h.error_code_from_exception(e);
				}

			}		
			};
			//// 0 parameters
			//template<class R>
			//struct vte<R()>{
			//static R vtable_entry_function(){
			//	helper h(pb);
			//	try{
			//		auto f = h.get_function();
			//		if(!f){
			//			return h.forward_to_runtime_parent();
			//		}
			//		return Derived::vtable_function(f,pb);
			//	} catch(std::exception& e){
			//		return h.error_code_from_exception(e);
			//	}

			//}		
			//};



		};

	}


	template<class Iface, int Id,class F1, class F2,class Derived,class FuncType = std::function<F1>>
	struct custom_cross_function{};


	// for size

	template<template<class> class Iface,int Id,class F1, class F2,class Derived,class FuncType>
	struct custom_cross_function<Iface<size_only>,Id,F1,F2,Derived,FuncType>{char a[1024];
	template<class T>
	custom_cross_function(T t){}
	typedef custom_cross_function base_t;

	};

	// for checksum

	template<template<class> class Iface,int Id,class F1, class F2,class Derived,class FuncType>
	struct custom_cross_function<Iface<checksum_only>,Id,F1,F2,Derived,FuncType>{ char a[1024*(Id+1+Iface<checksum_only>::base_sz)*(Id+1+Iface<checksum_only>::base_sz)];
	template<class T>
	custom_cross_function(T t){}
	typedef custom_cross_function base_t;

	};

	// For usage
	template<class User, template<class> class Iface, int Id,class F1, class F2,class Derived,class FuncType>
	struct custom_cross_function<Iface<User>,Id,F1,F2,Derived,FuncType>{
	private:
		portable_base* p_;
	public:
		typedef custom_cross_function base_t;
		enum{N = Iface<User>::base_sz + Id};
		typedef typename std::function<F1>::result_type ret;
		typedef typename detail::fn_ptr_helper<F2>::fn_ptr_t vtable_fn_ptr_t;

		custom_cross_function(Iface<User>* pi):p_(static_cast<User*>(pi)->get_portable_base()){}


		// 5 parameters
		template<class Parms,class Parms2, class Parms3, class Parms4, class Parms5>
		ret operator()(Parms p,Parms2 p2,Parms3 p3,Parms4 p4,Parms5 p5)const {
			if(p_){
				return static_cast<const Derived*>(this)->call_vtable_function(p,p2,p3,p4,p5);
			}
			else{
				throw error_pointer();
			}
		}
		// 4 parameters
		template<class Parms,class Parms2, class Parms3, class Parms4>
		ret operator()(Parms p,Parms2 p2,Parms3 p3,Parms4 p4)const {
			if(p_){
				return static_cast<const Derived*>(this)->call_vtable_function(p,p2,p3,p4);
			}
			else{
				throw error_pointer();
			}
		}
		// 3 parameters
		template<class Parms,class Parms2, class Parms3>
		ret operator()(Parms p,Parms2 p2,Parms3 p3)const {
			if(p_){
				return static_cast<const Derived*>(this)->call_vtable_function(p,p2,p3);
			}
			else{
				throw error_pointer();
			}
		}
		// 2 parameters
		template<class Parms,class Parms2>
		ret operator()(Parms p,Parms2 p2)const {
			if(p_){
				return static_cast<const Derived*>(this)->call_vtable_function(p,p2);
			}
			else{
				throw error_pointer();
			}
		}
		// 1 parameters
		template<class Parms>
		ret operator()(Parms p)const {
			if(p_){
				return static_cast<const Derived*>(this)->call_vtable_function(p);
			}
			else{
				throw error_pointer();
			}
		}
		// 0 parameters
		ret operator()()const {
			if(p_){
				return static_cast<const Derived*>(this)->call_vtable_function();
			}
			else{
				throw error_pointer();
			}
		}

	protected:

		vtable_fn_ptr_t get_vtable_fn()const{
			return reinterpret_cast<vtable_fn_ptr_t>(p_->vfptr[N]);
		}

		portable_base* get_portable_base()const{
			return p_;
		}

		void exception_from_error_code(error_code e)const{
			error_mapper<Iface>::mapper::exception_from_error_code(e);
		}
	};



	// For implementation
	template<template<class> class Iface, template<class> class T,int Id,class F1, class F2,class Derived,class FuncType>
	struct custom_cross_function<Iface<implement_interface<T>>,Id,F1,F2,Derived,FuncType>:public FuncType { // For empty base optimization in case FuncType is of 0 size
	private:
		portable_base* p_;
	public:
		typedef custom_cross_function base_t;

		enum{N = Iface<implement_interface<T>>::base_sz + Id};
	private:
		typedef typename detail::custom_function_vtable_functions<Iface,Derived,N,FuncType, F2>::template vte<F2> vtable_functions_t;
	public:

		custom_cross_function(Iface<implement_interface<T>>* pi):p_(static_cast<implement_interface<T>*>(pi)->get_portable_base()){
			auto vn = static_cast<vtable_n_base*>(p_);
			vn->set_data(N,static_cast<FuncType*>(this));
			vn->add(N,&vtable_functions_t::vtable_entry_function);

		}

		typedef typename std::function<F1>::result_type ret;

		typedef typename detail::fn_ptr_helper<F2>::fn_ptr_t vtable_fn_ptr_t;

		// 5 parameters
		template<class Parms,class Parms2, class Parms3, class Parms4, class Parms5>
		ret operator()(Parms p,Parms2 p2,Parms3 p3,Parms4 p4,Parms5 p5)const {
			if(p_){
				return static_cast<const Derived*>(this)->call_vtable_function(p,p2,p3,p4,p5);
			}
			else{
				throw error_pointer();
			}
		}
		// 4 parameters
		template<class Parms,class Parms2, class Parms3, class Parms4>
		ret operator()(Parms p,Parms2 p2,Parms3 p3,Parms4 p4)const {
			if(p_){
				return static_cast<const Derived*>(this)->call_vtable_function(p,p2,p3,p4);
			}
			else{
				throw error_pointer();
			}
		}
		// 3 parameters
		template<class Parms,class Parms2, class Parms3>
		ret operator()(Parms p,Parms2 p2,Parms3 p3)const {
			if(p_){
				return static_cast<const Derived*>(this)->call_vtable_function(p,p2,p3);
			}
			else{
				throw error_pointer();
			}
		}
		// 2 parameters
		template<class Parms,class Parms2>
		ret operator()(Parms p,Parms2 p2)const {
			if(p_){
				return static_cast<const Derived*>(this)->call_vtable_function(p,p2);
			}
			else{
				throw error_pointer();
			}
		}
		// 1 parameters
		template<class Parms>
		ret operator()(Parms p)const {
			if(p_){
				return static_cast<const Derived*>(this)->call_vtable_function(p);
			}
			else{
				throw error_pointer();
			}
		}
		// 0 parameters
		ret operator()()const {
			if(p_){
				return static_cast<const Derived*>(this)->call_vtable_function();
			}
			else{
				throw error_pointer();
			}
		}

		typedef detail::mem_fn_helper<F1> tm;
		template<class C, typename tm:: template inner<C,Iface,N>::MFT mf>
		void set_mem_fn (C* c){
			auto f = typename tm:: template inner<C,Iface,N>::get_func<mf>(c);
			set_function(f);
		}

	protected:

		template<class Func>
		void set_function(Func f){
			detail::get_function<N,FuncType>(p_) = f;
		}

		vtable_fn_ptr_t get_vtable_fn()const{
			return reinterpret_cast<vtable_fn_ptr_t>(p_->vfptr[N]);
		}

		portable_base* get_portable_base()const{
			return p_;
		};


	};







}



//          Copyright John R. Bandela 2012.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#ifndef cross_compiler_interface_
#define cross_compiler_interface_


// Disable some MSVC warnings
#pragma warning(push)
#pragma warning(disable:4996)
#pragma  warning(disable: 4099)
#pragma  warning(disable: 4351)


#include "implementation/cross_compiler_error_handling.hpp"

// Include Platform Specific
#ifdef _WIN32
#include "platform/Windows/platform_specific.hpp"
#endif // _WIN32
#ifdef __linux__
#include "platform/Linux/platform_specific.hpp"
#endif // __linux__

#include <functional>
#include <assert.h>
#include <cstddef>
#include <stdexcept>
#include <string>




namespace cross_compiler_interface{

	// Template for converting to/from regular types to cross-compiler compatible types 
	template<class T>
	struct cross_conversion{		
	};

	// Template for converting return types to/from regular types to cross-compiler compatible types 
	// by default use cross_conversion
	template<class T>
	struct cross_conversion_return{
		typedef cross_conversion<T> cc;
		typedef typename cc::original_type return_type;
		typedef typename cc::converted_type converted_type;

		static void initialize_return(return_type&, converted_type&){
			// do nothing
		}

		static void do_return(const return_type& r,converted_type& c){
			typedef cross_conversion<T> cc;
			c = cc::to_converted_type(r);
		}
		static void finalize_return(return_type& r,converted_type& c){
			r = cc::to_original_type(c);
		}

	};

	namespace detail{
		// Calling convention defined in platform specific header

		typedef  void(CROSS_CALL_CALLING_CONVENTION *ptr_fun_void_t)();
		extern "C"{
			struct  portable_base{
				ptr_fun_void_t* vfptr;
			};

		}
	}
	typedef detail::portable_base portable_base;

	// Make sure no padding
	static_assert(sizeof(portable_base)==sizeof(detail::ptr_fun_void_t*),"Padding in portable_base");



	namespace detail{

		// Helper functions to cast a vtable function to the correct type and call it
		template<class R, class Parms>
		R call(const ptr_fun_void_t pFun,Parms p){
			typedef R( CROSS_CALL_CALLING_CONVENTION *fun_t)(Parms);
			auto f = reinterpret_cast<fun_t>(pFun);
			return f(p);
		}
		// Helper functions to cast a vtable function to the correct type and call it
		template<class R, class Parms,class Parms2>
		R call(const ptr_fun_void_t pFun,Parms p,Parms2 p2){
			typedef R( CROSS_CALL_CALLING_CONVENTION *fun_t)(Parms,Parms2);
			auto f = reinterpret_cast<fun_t>(pFun);
			return f(p,p2);
		}
		// Helper functions to cast a vtable function to the correct type and call it
		template<class R, class Parms,class Parms2,class Parms3>
		R call(const ptr_fun_void_t pFun,Parms p,Parms2 p2,Parms3 p3){
			typedef R( CROSS_CALL_CALLING_CONVENTION *fun_t)(Parms,Parms2,Parms3);
			auto f = reinterpret_cast<fun_t>(pFun);
			return f(p,p2,p3);
		}

		// Helper functions to cast a vtable function to the correct type and call it
		template<class R, class Parms,class Parms2,class Parms3,class Parms4>
		R call(const ptr_fun_void_t pFun,Parms p,Parms2 p2,Parms3 p3,Parms4 p4){
			typedef R( CROSS_CALL_CALLING_CONVENTION *fun_t)(Parms,Parms2,Parms3,Parms4);
			auto f = reinterpret_cast<fun_t>(pFun);
			return f(p,p2,p3,p4);
		}
		// Helper functions to cast a vtable function to the correct type and call it
		template<class R, class Parms,class Parms2,class Parms3,class Parms4,class Parms5>
		R call(const ptr_fun_void_t pFun,Parms p,Parms2 p2,Parms3 p3,Parms4 p4,Parms5 p5){
			typedef R( CROSS_CALL_CALLING_CONVENTION *fun_t)(Parms,Parms2,Parms3,Parms4,Parms5);
			auto f = reinterpret_cast<fun_t>(pFun);
			return f(p,p2,p3,p4,p5);
		}

		template<class T>
		T& dummy_conversion(T& t){
			return t;
		}

	}



	// base class for vtable_n
	struct vtable_n_base:public portable_base{
		void** pdata;
		portable_base* runtime_parent_;
		vtable_n_base(void** p):pdata(p),runtime_parent_(0){}
		template<int n,class T>
		T* get_data()const{
			return static_cast<T*>(pdata[n]);
		}

		void set_data(int n,void* d){
			pdata[n] = d;
		}

		template<class F>
		void update(int n,F pfun){
			vfptr[n] = reinterpret_cast<detail::ptr_fun_void_t>(pfun);
		}

		template<class F>
		void add(int n,F pfun){
			// If you have an assertion here, you have a duplicated number in you interface
			assert(vfptr[n] == nullptr);
			update(n,pfun);
		}
	};

	// Our "vtable" definition
	template<int N>
	struct vtable_n:public vtable_n_base 
	{
	protected:
		detail::ptr_fun_void_t table_n[N];
		void* data[N];
		enum {sz = N};
		vtable_n():vtable_n_base(data),table_n(),data(){
			vfptr = &table_n[0];
		}

	public:
		portable_base* get_portable_base(){return this;}
		const portable_base* get_portable_base()const{return this;}

	};

	namespace detail{
		template<int N,class F>
		F& get_function(const portable_base* v){
			const vtable_n_base* vt = static_cast<const vtable_n_base*>(v);
			return *vt->template get_data<N,F>();
		}

		template<int N,class T>
		T* get_data(const portable_base* v){
			const vtable_n_base* vt = static_cast<const vtable_n_base*>(v);
			return vt->template get_data<N,T>();
		}

		template<bool bImp,template<class> class Iface, int N,class F>
		struct cross_function_implementation{};

		struct conversion_helper{ // Used to Help MSVC++ avoid Internal Compiler Error
			template<class Parm>
			static typename cross_conversion<Parm>::converted_type to_converted(const Parm& p){
				typedef cross_conversion<Parm> cc;
				return cc::to_converted_type(p);
			}
			template<class Parm>
			static typename cross_conversion<Parm>::original_type to_original(typename cross_conversion<Parm>::converted_type p){
				typedef cross_conversion<Parm> cc;
				return cc::to_original_type(p);
			}

		};


		template<template<class> class Iface, int N>
		struct call_adaptor{

			// Support up to 5 parameters
			template<class R,class Parms = null_t,class Parms2 = null_t,class Parms3 = null_t,class Parms4 = null_t,class Parms5 = null_t>
			struct vtable_caller{				
				static R call_vtable_func(const detail::ptr_fun_void_t pFun,const portable_base* v,Parms p,Parms2 p2,Parms3 p3,Parms4 p4,Parms5 p5){
					typedef cross_conversion_return<R> ccr;
					typedef typename ccr::converted_type cret_t;
					typename ccr::return_type r;
					cret_t cret;
					ccr::initialize_return(r,cret);
					auto ret =  detail::call<error_code,const portable_base*, cret_t*, typename cross_conversion<Parms>::converted_type,
						typename cross_conversion<Parms2>::converted_type,typename cross_conversion<Parms3>::converted_type,
						typename cross_conversion<Parms4>::converted_type,typename cross_conversion<Parms5>::converted_type>(pFun,
						v,&cret,conversion_helper::to_converted<Parms>(p),conversion_helper::to_converted<Parms2>(p2),conversion_helper::to_converted<Parms3>(p3),
						conversion_helper::to_converted<Parms4>(p4),conversion_helper::to_converted<Parms5>(p5));
					if(ret){
						error_mapper<Iface>::mapper::exception_from_error_code(ret);
					}
					ccr::finalize_return(r,cret);
					return r;
				}

			};
			// Support 4 parameters
			template<class R,class Parms,class Parms2,class Parms3,class Parms4>
			struct vtable_caller<R,Parms,Parms2,Parms3,Parms4,null_t>{				
				static R call_vtable_func(const detail::ptr_fun_void_t pFun,const portable_base* v,Parms p,Parms2 p2,Parms3 p3,Parms4 p4){
					typedef cross_conversion_return<R> ccr;
					typedef typename ccr::converted_type cret_t;
					typename ccr::return_type r;
					cret_t cret;
					ccr::initialize_return(r,cret);
					auto ret =  detail::call<error_code,const portable_base*, cret_t*, typename cross_conversion<Parms>::converted_type,
						typename cross_conversion<Parms2>::converted_type,typename cross_conversion<Parms3>::converted_type,
						typename cross_conversion<Parms4>::converted_type>(pFun,
						v,&cret,conversion_helper::to_converted<Parms>(p),conversion_helper::to_converted<Parms2>(p2),conversion_helper::to_converted<Parms3>(p3),
						conversion_helper::to_converted<Parms4>(p4));
					if(ret){
						error_mapper<Iface>::mapper::exception_from_error_code(ret);
					}
					ccr::finalize_return(r,cret);
					return r;
				}

			};
			// 3 Parameters
			template<class R,class Parms,class Parms2,class Parms3>
			struct vtable_caller<R,Parms,Parms2,Parms3,null_t,null_t>{				
				static R call_vtable_func(const detail::ptr_fun_void_t pFun,const portable_base* v,Parms p,Parms2 p2,Parms3 p3){
					typedef cross_conversion_return<R> ccr;
					typedef typename ccr::converted_type cret_t;
					typename ccr::return_type r;
					cret_t cret;
					ccr::initialize_return(r,cret);
					auto ret =  detail::call<error_code,const portable_base*, cret_t*, typename cross_conversion<Parms>::converted_type,
						typename cross_conversion<Parms2>::converted_type,typename cross_conversion<Parms3>::converted_type>(pFun,
						v,&cret,conversion_helper::to_converted<Parms>(p),conversion_helper::to_converted<Parms2>(p2),conversion_helper::to_converted<Parms3>(p3));
					if(ret){
						error_mapper<Iface>::mapper::exception_from_error_code(ret);
					}
					ccr::finalize_return(r,cret);
					return r;
				}

			};
			// 2 Parameters
			template<class R,class Parms,class Parms2>
			struct vtable_caller<R,Parms,Parms2,null_t,null_t,null_t>{				
				static R call_vtable_func(const detail::ptr_fun_void_t pFun,const portable_base* v,Parms p,Parms2 p2){
					typedef cross_conversion_return<R> ccr;
					typedef typename ccr::converted_type cret_t;
					typename ccr::return_type r;
					cret_t cret;
					ccr::initialize_return(r,cret);
					auto ret =  detail::call<error_code,const portable_base*, cret_t*, typename cross_conversion<Parms>::converted_type,
						typename cross_conversion<Parms2>::converted_type>(pFun,
						v,&cret,conversion_helper::to_converted<Parms>(p),conversion_helper::to_converted<Parms2>(p2));
					if(ret){
						error_mapper<Iface>::mapper::exception_from_error_code(ret);
					}
					ccr::finalize_return(r,cret);
					return r;
				}

			};

			// 1 Parameters
			template<class R,class Parms>
			struct vtable_caller<R,Parms,null_t,null_t,null_t,null_t>{				
				static R call_vtable_func(const detail::ptr_fun_void_t pFun,const portable_base* v,Parms p){
					typedef cross_conversion_return<R> ccr;
					typedef typename ccr::converted_type cret_t;
					typename ccr::return_type r;
					cret_t cret;
					ccr::initialize_return(r,cret);
					auto ret =  detail::call<error_code,const portable_base*, cret_t*, typename cross_conversion<Parms>::converted_type>(pFun,
						v,&cret,conversion_helper::to_converted<Parms>(p));
					if(ret){
						error_mapper<Iface>::mapper::exception_from_error_code(ret);
					}
					ccr::finalize_return(r,cret);
					return r;
				}

			};
			// 0 Parameters
			template<class R>
			struct vtable_caller<R,null_t,null_t,null_t,null_t,null_t>{				
				static R call_vtable_func(const detail::ptr_fun_void_t pFun,const portable_base* v){
					typedef cross_conversion_return<R> ccr;
					typedef typename ccr::converted_type cret_t;
					typename ccr::return_type r;
					cret_t cret;
					ccr::initialize_return(r,cret);
					auto ret =  detail::call<error_code,const portable_base*, cret_t*>(pFun,
						v,&cret);
					if(ret){
						error_mapper<Iface>::mapper::exception_from_error_code(ret);
					}
					ccr::finalize_return(r,cret);
					return r;
				}

			};

			// Void return type, 5 parameters
			template<class Parms,class Parms2,class Parms3,class Parms4,class Parms5>
			struct vtable_caller<void,Parms,Parms2,Parms3,Parms4,Parms5>{				
				static void call_vtable_func(const detail::ptr_fun_void_t pFun,const portable_base* v,Parms p,Parms2 p2,Parms3 p3,Parms4 p4,Parms5 p5){
					auto ret =  detail::call<error_code,const portable_base*,typename cross_conversion<Parms>::converted_type,
						typename cross_conversion<Parms2>::converted_type,typename cross_conversion<Parms3>::converted_type,
						typename cross_conversion<Parms4>::converted_type,typename cross_conversion<Parms5>::converted_type>(pFun,
						v,conversion_helper::to_converted<Parms>(p),conversion_helper::to_converted<Parms2>(p2),conversion_helper::to_converted<Parms3>(p3),
						conversion_helper::to_converted<Parms4>(p4),conversion_helper::to_converted<Parms5>(p5));
					if(ret){
						error_mapper<Iface>::mapper::exception_from_error_code(ret);
					}
				}

			};
			// Support 4 parameters
			template<class Parms,class Parms2,class Parms3,class Parms4>
			struct vtable_caller<void,Parms,Parms2,Parms3,Parms4,null_t>{				
				static void call_vtable_func(const detail::ptr_fun_void_t pFun,const portable_base* v,Parms p,Parms2 p2,Parms3 p3,Parms4 p4){
					auto ret =  detail::call<error_code,const portable_base*, typename cross_conversion<Parms>::converted_type,
						typename cross_conversion<Parms2>::converted_type,typename cross_conversion<Parms3>::converted_type,
						typename cross_conversion<Parms4>::converted_type>(pFun,
						v,conversion_helper::to_converted<Parms>(p),conversion_helper::to_converted<Parms2>(p2),conversion_helper::to_converted<Parms3>(p3),
						conversion_helper::to_converted<Parms4>(p4));
					if(ret){
						error_mapper<Iface>::mapper::exception_from_error_code(ret);
					}
				}

			};
			// 3 Parameters
			template<class Parms,class Parms2,class Parms3>
			struct vtable_caller<void,Parms,Parms2,Parms3,null_t,null_t>{				
				static void call_vtable_func(const detail::ptr_fun_void_t pFun,const portable_base* v,Parms p,Parms2 p2,Parms3 p3){
					auto ret =  detail::call<error_code,const portable_base*,typename cross_conversion<Parms>::converted_type,
						typename cross_conversion<Parms2>::converted_type,typename cross_conversion<Parms3>::converted_type>(pFun,
						v,conversion_helper::to_converted<Parms>(p),conversion_helper::to_converted<Parms2>(p2),conversion_helper::to_converted<Parms3>(p3));
					if(ret){
						error_mapper<Iface>::mapper::exception_from_error_code(ret);
					}
				}

			};
			// 2 Parameters
			template<class Parms,class Parms2>
			struct vtable_caller<void,Parms,Parms2,null_t,null_t,null_t>{				
				static void call_vtable_func(const detail::ptr_fun_void_t pFun,const portable_base* v,Parms p,Parms2 p2){
					auto ret =  detail::call<error_code,const portable_base*, typename cross_conversion<Parms>::converted_type,
						typename cross_conversion<Parms2>::converted_type>(pFun,
						v,conversion_helper::to_converted<Parms>(p),conversion_helper::to_converted<Parms2>(p2));
					if(ret){
						error_mapper<Iface>::mapper::exception_from_error_code(ret);
					}
				}

			};

			// 1 Parameters
			template<class Parms>
			struct vtable_caller<void,Parms,null_t,null_t,null_t,null_t>{				
				static void call_vtable_func(const detail::ptr_fun_void_t pFun,const portable_base* v,Parms p){
					auto ret =  detail::call<error_code,const portable_base*, typename cross_conversion<Parms>::converted_type>(pFun,
						v,conversion_helper::to_converted<Parms>(p));
					if(ret){
						error_mapper<Iface>::mapper::exception_from_error_code(ret);
					}
				}

			};
			// 0 Parameters
			template<>
			struct vtable_caller<void,null_t,null_t,null_t,null_t,null_t>{				
				static void call_vtable_func(const detail::ptr_fun_void_t pFun,const portable_base* v){
					auto ret =  detail::call<error_code,const portable_base*>(pFun,
						v);
					if(ret){
						error_mapper<Iface>::mapper::exception_from_error_code(ret);
					}
				}

			};

			// Support up to 5 parameters
			template<class R,class Parms = null_t,class Parms2 = null_t,class Parms3 = null_t,class Parms4 = null_t,class Parms5 = null_t>
			struct vtable_entry{
				typedef std::function<R(Parms,Parms2,Parms3,Parms4,Parms5)> fun_t;
				typedef cross_conversion_return<R> ccr;
				typedef error_code (CROSS_CALL_CALLING_CONVENTION * vt_entry_func)(const portable_base*,
					typename ccr::converted_type*,typename cross_conversion<Parms>::converted_type,
					typename cross_conversion<Parms2>::converted_type,typename cross_conversion<Parms3>::converted_type,
					typename cross_conversion<Parms4>::converted_type,typename cross_conversion<Parms5>::converted_type);

				static error_code CROSS_CALL_CALLING_CONVENTION func(const portable_base* v, typename ccr::converted_type* r,typename cross_conversion<Parms>::converted_type p,typename cross_conversion<Parms2>::converted_type p2,
					typename cross_conversion<Parms3>::converted_type p3,typename cross_conversion<Parms4>::converted_type p4,typename cross_conversion<Parms5>::converted_type p5){
						try{
							auto& f = detail::get_function<N,fun_t>(v);
							if(!f){
								// See if runtime inheritance present with parent
								const vtable_n_base* vt = static_cast<const vtable_n_base*>(v);
								if(vt->runtime_parent_){
									// call the parent
									// Use dummy conversion because MSVC does not like just p...
									return reinterpret_cast<vt_entry_func>(vt->runtime_parent_->vfptr[N])(vt->runtime_parent_,r,detail::dummy_conversion<typename cross_conversion<Parms>::converted_type>(p),
									detail::dummy_conversion<typename cross_conversion<Parms2>::converted_type>(p2),detail::dummy_conversion<typename cross_conversion<Parms3>::converted_type>(p3),	
								detail::dummy_conversion<typename cross_conversion<Parms4>::converted_type>(p4),detail::dummy_conversion<typename cross_conversion<Parms5>::converted_type>(p5));
							}
								else{
									return error_not_implemented::ec;
								}
							}
							ccr::do_return(f(conversion_helper::to_original<Parms>(p),conversion_helper::to_original<Parms2>(p2),
								conversion_helper::to_original<Parms3>(p3),conversion_helper::to_original<Parms4>(p4),conversion_helper::to_original<Parms5>(p5)),*r);
							return 0;
						} catch(std::exception& e){
							return error_mapper<Iface>::mapper::error_code_from_exception(e);
						}
				}
			};
			// 4 Parameters
			template<class R,class Parms,class Parms2,class Parms3,class Parms4>
			struct vtable_entry<R,Parms,Parms2,Parms3,Parms4,null_t>{
				typedef std::function<R(Parms,Parms2,Parms3,Parms4)> fun_t;
				typedef cross_conversion_return<R> ccr;

				typedef typename cross_conversion<Parms>::converted_type ct1;
				typedef typename cross_conversion<Parms2>::converted_type ct2;
				typedef typename cross_conversion<Parms3>::converted_type ct3;
				typedef typename cross_conversion<Parms4>::converted_type ct4;
				//typedef typename cross_conversion<Parms5>::converted_type ct5;
				typedef error_code (CROSS_CALL_CALLING_CONVENTION * vt_entry_func)(const portable_base*,
					typename ccr::converted_type*,ct1,ct2,ct3,
					ct4);

				static error_code CROSS_CALL_CALLING_CONVENTION func(const portable_base* v, typename ccr::converted_type* r,typename cross_conversion<Parms>::converted_type p,typename cross_conversion<Parms2>::converted_type p2,
					typename cross_conversion<Parms3>::converted_type p3,typename cross_conversion<Parms4>::converted_type p4){
						try{
							auto& f = detail::get_function<N,fun_t>(v);
							if(!f){
								// See if runtime inheritance present with parent
								const vtable_n_base* vt = static_cast<const vtable_n_base*>(v);
								if(vt->runtime_parent_){
									// call the parent
									// Use dummy conversion because MSVC does not like just p...
									return reinterpret_cast<vt_entry_func>(vt->runtime_parent_->vfptr[N])(vt->runtime_parent_,r,detail::dummy_conversion<typename cross_conversion<Parms>::converted_type>(p),
									detail::dummy_conversion<typename cross_conversion<Parms2>::converted_type>(p2),detail::dummy_conversion<typename cross_conversion<Parms3>::converted_type>(p3),	
								detail::dummy_conversion<typename cross_conversion<Parms4>::converted_type>(p4));
							}
								else{
									return error_not_implemented::ec;
								}
							}
							ccr::do_return(f(conversion_helper::to_original<Parms>(p),conversion_helper::to_original<Parms2>(p2),conversion_helper::to_original<Parms3>(p3),conversion_helper::to_original<Parms4>(p4)),*r);
							return 0;
						} catch(std::exception& e){
							return error_mapper<Iface>::mapper::error_code_from_exception(e);
						}
				}
			};
			// 3 Parameters
			template<class R,class Parms,class Parms2,class Parms3>
			struct vtable_entry<R,Parms,Parms2,Parms3,null_t,null_t>{
				typedef std::function<R(Parms,Parms2,Parms3)> fun_t;
				typedef cross_conversion_return<R> ccr;
				typedef error_code (CROSS_CALL_CALLING_CONVENTION * vt_entry_func)(const portable_base*,
					typename ccr::converted_type*,typename cross_conversion<Parms>::converted_type,
					typename cross_conversion<Parms2>::converted_type,typename cross_conversion<Parms3>::converted_type);

				static error_code CROSS_CALL_CALLING_CONVENTION func(const portable_base* v, typename ccr::converted_type* r,typename cross_conversion<Parms>::converted_type p,typename cross_conversion<Parms2>::converted_type p2,
					typename cross_conversion<Parms3>::converted_type p3){
						try{
							auto& f = detail::get_function<N,fun_t>(v);
							if(!f){
								// See if runtime inheritance present with parent
								const vtable_n_base* vt = static_cast<const vtable_n_base*>(v);
								if(vt->runtime_parent_){
									// call the parent
									// Use dummy conversion because MSVC does not like just p...
									return reinterpret_cast<vt_entry_func>(vt->runtime_parent_->vfptr[N])(vt->runtime_parent_,r,detail::dummy_conversion<typename cross_conversion<Parms>::converted_type>(p),
									detail::dummy_conversion<typename cross_conversion<Parms2>::converted_type>(p2),detail::dummy_conversion<typename cross_conversion<Parms3>::converted_type>(p3));
							}
								else{
									return error_not_implemented::ec;
								}
							}
							ccr::do_return(f(conversion_helper::to_original<Parms>(p),conversion_helper::to_original<Parms2>(p2),conversion_helper::to_original<Parms3>(p3)),*r);
							return 0;
						} catch(std::exception& e){
							return error_mapper<Iface>::mapper::error_code_from_exception(e);
						}
				}
			};

			// 2 Parameters
			template<class R,class Parms,class Parms2>
			struct vtable_entry<R,Parms,Parms2,null_t,null_t,null_t>{
				typedef std::function<R(Parms,Parms2)> fun_t;
				typedef cross_conversion_return<R> ccr;
				typedef error_code (CROSS_CALL_CALLING_CONVENTION * vt_entry_func)(const portable_base*,
					typename ccr::converted_type*,typename cross_conversion<Parms>::converted_type,
					typename cross_conversion<Parms2>::converted_type);

				static error_code CROSS_CALL_CALLING_CONVENTION func(const portable_base* v, typename ccr::converted_type* r,typename cross_conversion<Parms>::converted_type p,typename cross_conversion<Parms2>::converted_type p2){
						try{
							auto& f = detail::get_function<N,fun_t>(v);
							if(!f){
								// See if runtime inheritance present with parent
								const vtable_n_base* vt = static_cast<const vtable_n_base*>(v);
								if(vt->runtime_parent_){
									// call the parent
									// Use dummy conversion because MSVC does not like just p...
									return reinterpret_cast<vt_entry_func>(vt->runtime_parent_->vfptr[N])(vt->runtime_parent_,r,detail::dummy_conversion<typename cross_conversion<Parms>::converted_type>(p),
									detail::dummy_conversion<typename cross_conversion<Parms2>::converted_type>(p2));
							}
								else{
									return error_not_implemented::ec;
								}
							}
							ccr::do_return(f(conversion_helper::to_original<Parms>(p),conversion_helper::to_original<Parms2>(p2)),*r);
							return 0;
						} catch(std::exception& e){
							return error_mapper<Iface>::mapper::error_code_from_exception(e);
						}
				}
			};

			// 1 Parameters
			template<class R,class Parms>
			struct vtable_entry<R,Parms,null_t,null_t,null_t,null_t>{
				typedef std::function<R(Parms)> fun_t;
				typedef cross_conversion_return<R> ccr;
				typedef error_code (CROSS_CALL_CALLING_CONVENTION * vt_entry_func)(const portable_base*,
					typename ccr::converted_type*,typename cross_conversion<Parms>::converted_type);

				static error_code CROSS_CALL_CALLING_CONVENTION func(const portable_base* v, typename ccr::converted_type* r,typename cross_conversion<Parms>::converted_type p){
						try{
							auto& f = detail::get_function<N,fun_t>(v);
							if(!f){
								// See if runtime inheritance present with parent
								const vtable_n_base* vt = static_cast<const vtable_n_base*>(v);
								if(vt->runtime_parent_){
									// call the parent
									// Use dummy conversion because MSVC does not like just p...
									return reinterpret_cast<vt_entry_func>(vt->runtime_parent_->vfptr[N])(vt->runtime_parent_,r,detail::dummy_conversion<typename cross_conversion<Parms>::converted_type>(p));
							}
								else{
									return error_not_implemented::ec;
								}
							}
							ccr::do_return(f(conversion_helper::to_original<Parms>(p)),*r);
							return 0;
						} catch(std::exception& e){
							return error_mapper<Iface>::mapper::error_code_from_exception(e);
						}
				}
			};

			// 0 Parameters
			template<class R>
			struct vtable_entry<R,null_t,null_t,null_t,null_t,null_t>{
				typedef std::function<R()> fun_t;
				typedef cross_conversion_return<R> ccr;
				typedef error_code (CROSS_CALL_CALLING_CONVENTION * vt_entry_func)(const portable_base*,
					typename ccr::converted_type*);

				static error_code CROSS_CALL_CALLING_CONVENTION func(const portable_base* v, typename ccr::converted_type* r){
						try{
							auto& f = detail::get_function<N,fun_t>(v);
							if(!f){
								// See if runtime inheritance present with parent
								const vtable_n_base* vt = static_cast<const vtable_n_base*>(v);
								if(vt->runtime_parent_){
									// call the parent
									// Use dummy conversion because MSVC does not like just p...
									return reinterpret_cast<vt_entry_func>(vt->runtime_parent_->vfptr[N])(vt->runtime_parent_,r);
							}
								else{
									return error_not_implemented::ec;
								}
							}
							ccr::do_return(f(),*r);
							return 0;
						} catch(std::exception& e){
							return error_mapper<Iface>::mapper::error_code_from_exception(e);
						}
				}
			};

			//template<class... Parms>
			//struct vtable_entry<void,Parms...>{
			//	typedef std::function<void(Parms...)> fun_t;
			//	typedef error_code (CROSS_CALL_CALLING_CONVENTION * vt_entry_func)(const portable_base*,
			//		typename cross_conversion<Parms>::converted_type...);

			//	static error_code CROSS_CALL_CALLING_CONVENTION func(const portable_base* v, typename cross_conversion<Parms>::converted_type... p){
			//		using namespace std; // Workaround for MSVC bug http://connect.microsoft.com/VisualStudio/feedback/details/772001/codename-milan-c-11-compilation-issue#details
			//		// See also http://connect.microsoft.com/VisualStudio/feedback/details/769988/codename-milan-total-mess-up-with-variadic-templates-and-namespaces
			//		try{
			//			auto& f = detail::get_function<N,fun_t>(v);
			//			if(!f){
			//				// See if runtime inheritance present with parent
			//				const vtable_n_base* vt = static_cast<const vtable_n_base*>(v);
			//				if(vt->runtime_parent_){
			//					// call the parent
			//					return reinterpret_cast<vt_entry_func>(vt->runtime_parent_->vfptr[N])(vt->runtime_parent_,detail::dummy_conversion<typename cross_conversion<Parms>::converted_type>(p)...);
			//				}
			//				else{
			//					return error_not_implemented::ec;
			//				}
			//			}

			//			f(conversion_helper::to_original<Parms>(p)...);
			//			return 0;
			//		} catch(std::exception& e){
			//			return error_mapper<Iface>::mapper::error_code_from_exception(e);
			//		}
			//	}
			//};

			// Support up to 5 parameters
			template<class Parms,class Parms2,class Parms3,class Parms4,class Parms5>
			struct vtable_entry<void,Parms,Parms2,Parms3,Parms4,Parms5>{
				typedef std::function<void(Parms,Parms2,Parms3,Parms4,Parms5)> fun_t;
				typedef error_code (CROSS_CALL_CALLING_CONVENTION * vt_entry_func)(const portable_base*,typename cross_conversion<Parms>::converted_type,
					typename cross_conversion<Parms2>::converted_type,typename cross_conversion<Parms3>::converted_type,
					typename cross_conversion<Parms4>::converted_type,typename cross_conversion<Parms5>::converted_type);

				static error_code CROSS_CALL_CALLING_CONVENTION func(const portable_base* v, typename cross_conversion<Parms>::converted_type p,typename cross_conversion<Parms2>::converted_type p2,
					typename cross_conversion<Parms3>::converted_type p3,typename cross_conversion<Parms4>::converted_type p4,typename cross_conversion<Parms5>::converted_type p5){
						try{
							auto& f = detail::get_function<N,fun_t>(v);
							if(!f){
								// See if runtime inheritance present with parent
								const vtable_n_base* vt = static_cast<const vtable_n_base*>(v);
								if(vt->runtime_parent_){
									// call the parent
									// Use dummy conversion because MSVC does not like just p...
									return reinterpret_cast<vt_entry_func>(vt->runtime_parent_->vfptr[N])(vt->runtime_parent_,detail::dummy_conversion<typename cross_conversion<Parms>::converted_type>(p),
									detail::dummy_conversion<typename cross_conversion<Parms2>::converted_type>(p2),detail::dummy_conversion<typename cross_conversion<Parms3>::converted_type>(p3),	
								detail::dummy_conversion<typename cross_conversion<Parms4>::converted_type>(p4),detail::dummy_conversion<typename cross_conversion<Parms5>::converted_type>(p5));
							}
								else{
									return error_not_implemented::ec;
								}
							}
							f(conversion_helper::to_original<Parms>(p),conversion_helper::to_original<Parms2>(p2),
								conversion_helper::to_original<Parms3>(p3),conversion_helper::to_original<Parms4>(p4),conversion_helper::to_original<Parms5>(p5));
							return 0;
						} catch(std::exception& e){
							return error_mapper<Iface>::mapper::error_code_from_exception(e);
						}
				}
			};
			// 4 Parameters
			template<class Parms,class Parms2,class Parms3,class Parms4>
			struct vtable_entry<void,Parms,Parms2,Parms3,Parms4,null_t>{
				typedef std::function<void(Parms,Parms2,Parms3,Parms4)> fun_t;
				typedef typename cross_conversion<Parms>::converted_type ct1;
				typedef typename cross_conversion<Parms2>::converted_type ct2;
				typedef typename cross_conversion<Parms3>::converted_type ct3;
				typedef typename cross_conversion<Parms4>::converted_type ct4;
				//typedef typename cross_conversion<Parms5>::converted_type ct5;
				typedef error_code (CROSS_CALL_CALLING_CONVENTION * vt_entry_func)(const portable_base*,
					ct1,ct2,ct3,ct4
					);

				static error_code CROSS_CALL_CALLING_CONVENTION func(const portable_base* v, typename cross_conversion<Parms>::converted_type p,typename cross_conversion<Parms2>::converted_type p2,
					typename cross_conversion<Parms3>::converted_type p3,typename cross_conversion<Parms4>::converted_type p4){
						try{
							auto& f = detail::get_function<N,fun_t>(v);
							if(!f){
								// See if runtime inheritance present with parent
								const vtable_n_base* vt = static_cast<const vtable_n_base*>(v);
								if(vt->runtime_parent_){
									// call the parent
									// Use dummy conversion because MSVC does not like just p...
									return reinterpret_cast<vt_entry_func>(vt->runtime_parent_->vfptr[N])(vt->runtime_parent_,detail::dummy_conversion<typename cross_conversion<Parms>::converted_type>(p),
									detail::dummy_conversion<typename cross_conversion<Parms2>::converted_type>(p2),detail::dummy_conversion<typename cross_conversion<Parms3>::converted_type>(p3),	
								detail::dummy_conversion<typename cross_conversion<Parms4>::converted_type>(p4));
							}
								else{
									return error_not_implemented::ec;
								}
							}
							f(conversion_helper::to_original<Parms>(p),conversion_helper::to_original<Parms2>(p2),conversion_helper::to_original<Parms3>(p3),conversion_helper::to_original<Parms4>(p4));
							return 0;
						} catch(std::exception& e){
							return error_mapper<Iface>::mapper::error_code_from_exception(e);
						}
				}
			};
			// 3 Parameters
			template<class Parms,class Parms2,class Parms3>
			struct vtable_entry<void,Parms,Parms2,Parms3,null_t,null_t>{
				typedef std::function<void(Parms,Parms2,Parms3)> fun_t;
				typedef error_code (CROSS_CALL_CALLING_CONVENTION * vt_entry_func)(const portable_base*,
					typename cross_conversion<Parms>::converted_type,
					typename cross_conversion<Parms2>::converted_type,typename cross_conversion<Parms3>::converted_type);

				static error_code CROSS_CALL_CALLING_CONVENTION func(const portable_base* v, typename cross_conversion<Parms>::converted_type p,typename cross_conversion<Parms2>::converted_type p2,
					typename cross_conversion<Parms3>::converted_type p3){
						try{
							auto& f = detail::get_function<N,fun_t>(v);
							if(!f){
								// See if runtime inheritance present with parent
								const vtable_n_base* vt = static_cast<const vtable_n_base*>(v);
								if(vt->runtime_parent_){
									// call the parent
									// Use dummy conversion because MSVC does not like just p...
									return reinterpret_cast<vt_entry_func>(vt->runtime_parent_->vfptr[N])(vt->runtime_parent_,detail::dummy_conversion<typename cross_conversion<Parms>::converted_type>(p),
									detail::dummy_conversion<typename cross_conversion<Parms2>::converted_type>(p2),detail::dummy_conversion<typename cross_conversion<Parms3>::converted_type>(p3));
							}
								else{
									return error_not_implemented::ec;
								}
							}
							f(conversion_helper::to_original<Parms>(p),conversion_helper::to_original<Parms2>(p2),conversion_helper::to_original<Parms3>(p3));
							return 0;
						} catch(std::exception& e){
							return error_mapper<Iface>::mapper::error_code_from_exception(e);
						}
				}
			};

			// 2 Parameters
			template<class Parms,class Parms2>
			struct vtable_entry<void,Parms,Parms2,null_t,null_t,null_t>{
				typedef std::function<void(Parms,Parms2)> fun_t;
				typedef error_code (CROSS_CALL_CALLING_CONVENTION * vt_entry_func)(const portable_base*,
					typename cross_conversion<Parms>::converted_type,
					typename cross_conversion<Parms2>::converted_type);

				static error_code CROSS_CALL_CALLING_CONVENTION func(const portable_base* v, typename cross_conversion<Parms>::converted_type p,typename cross_conversion<Parms2>::converted_type p2){
						try{
							auto& f = detail::get_function<N,fun_t>(v);
							if(!f){
								// See if runtime inheritance present with parent
								const vtable_n_base* vt = static_cast<const vtable_n_base*>(v);
								if(vt->runtime_parent_){
									// call the parent
									// Use dummy conversion because MSVC does not like just p...
									return reinterpret_cast<vt_entry_func>(vt->runtime_parent_->vfptr[N])(vt->runtime_parent_,detail::dummy_conversion<typename cross_conversion<Parms>::converted_type>(p),
									detail::dummy_conversion<typename cross_conversion<Parms2>::converted_type>(p2));
							}
								else{
									return error_not_implemented::ec;
								}
							}
							f(conversion_helper::to_original<Parms>(p),conversion_helper::to_original<Parms2>(p2));
							return 0;
						} catch(std::exception& e){
							return error_mapper<Iface>::mapper::error_code_from_exception(e);
						}
				}
			};

			// 1 Parameters
			template<class Parms>
			struct vtable_entry<void,Parms,null_t,null_t,null_t,null_t>{
				typedef std::function<void(Parms)> fun_t;
				typedef error_code (CROSS_CALL_CALLING_CONVENTION * vt_entry_func)(const portable_base*,
					typename cross_conversion<Parms>::converted_type);

				static error_code CROSS_CALL_CALLING_CONVENTION func(const portable_base* v, typename cross_conversion<Parms>::converted_type p){
						try{
							auto& f = detail::get_function<N,fun_t>(v);
							if(!f){
								// See if runtime inheritance present with parent
								const vtable_n_base* vt = static_cast<const vtable_n_base*>(v);
								if(vt->runtime_parent_){
									// call the parent
									// Use dummy conversion because MSVC does not like just p...
									return reinterpret_cast<vt_entry_func>(vt->runtime_parent_->vfptr[N])(vt->runtime_parent_,detail::dummy_conversion<typename cross_conversion<Parms>::converted_type>(p));
							}
								else{
									return error_not_implemented::ec;
								}
							}
							f(conversion_helper::to_original<Parms>(p));
							return 0;
						} catch(std::exception& e){
							return error_mapper<Iface>::mapper::error_code_from_exception(e);
						}
				}
			};

			// 0 Parameters
			template<>
			struct vtable_entry<void,null_t,null_t,null_t,null_t,null_t>{
				typedef std::function<void()> fun_t;
				typedef error_code (CROSS_CALL_CALLING_CONVENTION * vt_entry_func)(const portable_base*);

				static error_code CROSS_CALL_CALLING_CONVENTION func(const portable_base* v){
						try{
							auto& f = detail::get_function<N,fun_t>(v);
							if(!f){
								// See if runtime inheritance present with parent
								const vtable_n_base* vt = static_cast<const vtable_n_base*>(v);
								if(vt->runtime_parent_){
									// call the parent
									// Use dummy conversion because MSVC does not like just p...
									return reinterpret_cast<vt_entry_func>(vt->runtime_parent_->vfptr[N])(vt->runtime_parent_);
							}
								else{
									return error_not_implemented::ec;
								}
							}
							f();
							return 0;
						} catch(std::exception& e){
							return error_mapper<Iface>::mapper::error_code_from_exception(e);
						}
				}
			};

			//template<class ... Parms>
			//struct vtable_entry_fast{

			//	template<class C, class MF, MF mf, class R>
			//	static error_code CROSS_CALL_CALLING_CONVENTION func(const portable_base* v, typename cross_conversion_return<R>::converted_type* r,typename cross_conversion<Parms>::converted_type... p){
			//		using namespace std; // Workaround for MSVC bug http://connect.microsoft.com/VisualStudio/feedback/details/772001/codename-milan-c-11-compilation-issue#details

			//		typedef cross_conversion_return<R> ccr;

			//		try{
			//			C* f = detail::get_data<N,C>(v);
			//			ccr::do_return((f->*mf)(conversion_helper::to_original<Parms>(p)...),*r);
			//			return 0;
			//		} catch(std::exception& e){
			//			return error_mapper<Iface>::mapper::error_code_from_exception(e);
			//		}
			//	}
			//};

			//template<class ... Parms>
			//struct vtable_entry_fast_void{

			//	template<class C, class MF, MF mf, class R>
			//	static error_code CROSS_CALL_CALLING_CONVENTION func(const portable_base* v, typename cross_conversion<Parms>::converted_type... p){
			//		using namespace std; // Workaround for MSVC bug http://connect.microsoft.com/VisualStudio/feedback/details/772001/codename-milan-c-11-compilation-issue#details


			//		try{
			//			C* f = detail::get_data<N,C>(v);
			//			(f->*mf)(conversion_helper::to_original<Parms>(p)...);
			//			return 0;
			//		} catch(std::exception& e){
			//			return error_mapper<Iface>::mapper::error_code_from_exception(e);
			//		}
			//	}
			//};

		};


		template<bool bImp, template<class> class Iface, int N,class R, class Parms=null_t,class Parms2=null_t,class Parms3=null_t,class Parms4=null_t,class Parms5=null_t>
		struct cross_function_implementation_base{
			portable_base* p_;
			R operator()(Parms p,Parms2 p2,Parms3 p3,Parms4 p4,Parms5 p5)const{

				if(p_){

					using namespace std; // Workaround for MSVC bug http://connect.microsoft.com/VisualStudio/feedback/details/772001/codename-milan-c-11-compilation-issue#details
					// See also http://connect.microsoft.com/VisualStudio/feedback/details/769988/codename-milan-total-mess-up-with-variadic-templates-and-namespaces
					typedef typename call_adaptor<Iface,N>::template vtable_caller<R,Parms,Parms2,Parms3,Parms4,Parms5> adapter;
					return adapter::call_vtable_func(p_->vfptr[N],p_,p,p2,p3,p4,p5);

				}
				else{
					throw error_pointer();
				}

			}
			R operator()(Parms p,Parms2 p2,Parms3 p3,Parms4 p4)const{

				if(p_){

					using namespace std; // Workaround for MSVC bug http://connect.microsoft.com/VisualStudio/feedback/details/772001/codename-milan-c-11-compilation-issue#details
					// See also http://connect.microsoft.com/VisualStudio/feedback/details/769988/codename-milan-total-mess-up-with-variadic-templates-and-namespaces
					typedef typename call_adaptor<Iface,N>::template vtable_caller<R,Parms,Parms2,Parms3,Parms4> adapter;
					return adapter::call_vtable_func(p_->vfptr[N],p_,p,p2,p3,p4);

				}
				else{
					throw error_pointer();
				}

			}

			R operator()(Parms p,Parms2 p2,Parms3 p3)const{

				if(p_){

					using namespace std; // Workaround for MSVC bug http://connect.microsoft.com/VisualStudio/feedback/details/772001/codename-milan-c-11-compilation-issue#details
					// See also http://connect.microsoft.com/VisualStudio/feedback/details/769988/codename-milan-total-mess-up-with-variadic-templates-and-namespaces
					typedef typename call_adaptor<Iface,N>::template vtable_caller<R,Parms,Parms2,Parms3> adapter;
					return adapter::call_vtable_func(p_->vfptr[N],p_,p,p2,p3);

				}
				else{
					throw error_pointer();
				}

			}
			R operator()(Parms p,Parms2 p2)const{

				if(p_){

					using namespace std; // Workaround for MSVC bug http://connect.microsoft.com/VisualStudio/feedback/details/772001/codename-milan-c-11-compilation-issue#details
					// See also http://connect.microsoft.com/VisualStudio/feedback/details/769988/codename-milan-total-mess-up-with-variadic-templates-and-namespaces
					typedef typename call_adaptor<Iface,N>::template vtable_caller<R,Parms,Parms2> adapter;
					return adapter::call_vtable_func(p_->vfptr[N],p_,p,p2);

				}
				else{
					throw error_pointer();
				}

			}
			R operator()(Parms p)const{

				if(p_){

					using namespace std; // Workaround for MSVC bug http://connect.microsoft.com/VisualStudio/feedback/details/772001/codename-milan-c-11-compilation-issue#details
					// See also http://connect.microsoft.com/VisualStudio/feedback/details/769988/codename-milan-total-mess-up-with-variadic-templates-and-namespaces
					typedef typename call_adaptor<Iface,N>::template vtable_caller<R,Parms> adapter;
					return adapter::call_vtable_func(p_->vfptr[N],p_,p);

				}
				else{
					throw error_pointer();
				}

			}
			R operator()()const{

				if(p_){

					using namespace std; // Workaround for MSVC bug http://connect.microsoft.com/VisualStudio/feedback/details/772001/codename-milan-c-11-compilation-issue#details
					// See also http://connect.microsoft.com/VisualStudio/feedback/details/769988/codename-milan-total-mess-up-with-variadic-templates-and-namespaces
					typedef typename call_adaptor<Iface,N>::template vtable_caller<R> adapter;
					return adapter::call_vtable_func(p_->vfptr[N],p_);

				}
				else{
					throw error_pointer();
				}

			}

			cross_function_implementation_base(portable_base* v):p_(v){}
		};

		template<bool b,template<class> class Iface,int N,class F>
		struct f_helper{};

		template<template<class> class Iface,bool b,int N, class R>
		struct f_helper<b,Iface,N,R()>{
			typedef std::function<R()> fun_t;
			typedef cross_function_implementation_base<b,Iface,N,R> imp_base;
			typedef typename detail::call_adaptor<Iface,N> ::template vtable_entry<R> vte_t;
			typedef R return_type;

		};	
		template<template<class> class Iface,bool b,int N, class R,class Parms>
		struct f_helper<b,Iface,N,R(Parms)>{
			typedef std::function<R(Parms)> fun_t;
			typedef cross_function_implementation_base<b,Iface,N,R,Parms> imp_base;
			typedef detail::call_adaptor<Iface,N> ca_t;
			typedef typename ca_t::template vtable_entry<R,Parms> vte_t;
			typedef R return_type;

		};			template<template<class> class Iface,bool b,int N, class R,class Parms,class Parms2>
		struct f_helper<b,Iface,N,R(Parms,Parms2)>{
			typedef std::function<R(Parms,Parms2)> fun_t;
			typedef cross_function_implementation_base<b,Iface,N,R,Parms,Parms2> imp_base;
			typedef typename call_adaptor<Iface,N>::template vtable_entry<R,Parms,Parms2> vte_t;
			typedef R return_type;

		};

		template<template<class> class Iface,bool b,int N, class R,class Parms,class Parms2,class Parms3>
		struct f_helper<b,Iface,N,R(Parms,Parms2,Parms3)>{
			typedef std::function<R(Parms,Parms2,Parms3)> fun_t;
			typedef cross_function_implementation_base<b,Iface,N,R,Parms,Parms2,Parms3> imp_base;
			typedef typename call_adaptor<Iface,N>::template vtable_entry<R,Parms,Parms2,Parms3> vte_t;
			typedef R return_type;

		};	
		
		template<template<class> class Iface,bool b,int N, class R,class Parms,class Parms2,class Parms3,class Parms4>
		struct f_helper<b,Iface,N,R(Parms,Parms2,Parms3,Parms4)>{
			typedef std::function<R(Parms,Parms2,Parms3,Parms4)> fun_t;
			typedef cross_function_implementation_base<b,Iface,N,R,Parms,Parms2,Parms3,Parms4> imp_base;
			typedef typename call_adaptor<Iface,N>::template vtable_entry<R,Parms,Parms2,Parms3,Parms4> vte_t;
			typedef R return_type;

		};	
		template<template<class> class Iface,bool b,int N, class R,class Parms,class Parms2,class Parms3,class Parms4,class Parms5>
		struct f_helper<b,Iface,N,R(Parms,Parms2,Parms3,Parms4,Parms5)>{
			typedef std::function<R(Parms,Parms2,Parms3,Parms4,Parms5)> fun_t;
			typedef cross_function_implementation_base<b,Iface,N,R,Parms,Parms2,Parms3,Parms4,Parms5> imp_base;
			typedef typename call_adaptor<Iface,N>::template vtable_entry<R,Parms,Parms2,Parms3,Parms4,Parms5> vte_t;
			typedef R return_type;

		};	
		template<template<class> class Iface, int N,class F>
		struct cross_function_implementation<true, Iface,N,F>
			:public f_helper<true,Iface,N,F>::imp_base,
			public f_helper<true,Iface,N,F>::vte_t
		{ //Implementation
			typedef typename f_helper<true,Iface,N,F>::return_type return_type;
			typedef typename f_helper<true,Iface,N,F>::fun_t fun_t;
			fun_t func_;
			typedef typename f_helper<true,Iface,N,F>::imp_base base_t;
			cross_function_implementation(portable_base* p):base_t(p){
				auto vn = static_cast<vtable_n_base*>(p);
				vn->set_data(N,&func_);
				vn->add(N,cross_function_implementation::func);
			}

			template<class F>
			static void set_function(cross_function_implementation& cfi,F f){
				cfi.func_ = f;
			}

		};




		template<template<class> class Iface, int N,class F>
		struct cross_function_implementation<false, Iface,N,F>
			:public f_helper<false,Iface,N,F>::imp_base
		{ //Usage

			typedef typename f_helper<false,Iface,N,F>::return_type return_type;
			typedef typename f_helper<false,Iface,N,F>::fun_t fun_t;
			typedef typename f_helper<false,Iface,N,F>::imp_base base_t;

			cross_function_implementation(portable_base* p):
			base_t(p){}

		};

	}
	template<template<class> class Iface>
	struct implement_interface;


	template<class Iface, int Id,class F>
	struct cross_function{};


	struct size_only{};
	struct checksum_only{};

	// size only
	template<template<class> class Iface,int Id,class F>
	struct cross_function<Iface<size_only>,Id,F>{char a[1024];
	template<class T>
	cross_function(T t){}


	};
	// checksum only
	template<template<class> class Iface,int Id,class F>
	struct cross_function<Iface<checksum_only>,Id,F>{char a[1024*(Id+1+Iface<checksum_only>::base_sz)*(Id+1+Iface<checksum_only>::base_sz)];
	template<class T>
	cross_function(T t){}

	};


	template< class User,template<class> class Iface,int Id,class F>
	struct cross_function<Iface<User>,Id,F>:public detail::cross_function_implementation<false,Iface,Id + Iface<User>::base_sz,F>{
		enum{N = Id + Iface<User>::base_sz};
		cross_function(Iface<User>* pi):detail::cross_function_implementation<false,Iface,N,F>(static_cast<User*>(pi)->get_portable_base()){
		}


	};	


	namespace detail{

		// MSVC Milan has trouble with variadic templates
		// and mem_fn. We use this template to help us with mem_fn

		template<class F>
		struct mem_fn_helper{};
		template<class R>
		struct mem_fn_helper<R()>
		{
			template<class C,template<class>class Iface, int N>
			struct inner{

				typedef R (C::*MFT)();

				typedef R ret_t;
				template<MFT mf>
				static std::function<R()> get_func(C* c){
					return [c](){
						return (c->*mf)();
					};
				}

			};
		};
		template<class R,class Parms>
		struct mem_fn_helper<R(Parms)>
		{
			template<class C,template<class>class Iface, int N>
			struct inner{

				typedef R (C::*MFT)(Parms);

				typedef R ret_t;

				template<MFT mf>
				static std::function<R(Parms)> get_func(C* c){
					return [c](Parms p){
						return (c->*mf)(p);
					};
				}

			};
		};
		template<class R,class Parms,class Parms2>
		struct mem_fn_helper<R(Parms,Parms2)>
		{
			template<class C,template<class>class Iface, int N>
			struct inner{

				typedef R (C::*MFT)(Parms,Parms2);

				typedef R ret_t;
				template<MFT mf>
				static std::function<R(Parms,Parms2)> get_func(C* c){
					return [c](Parms p,Parms2 p2){
						return (c->*mf)(p,p2);
					};
				}

			};
		};
		template<class R,class Parms,class Parms2,class Parms3>
		struct mem_fn_helper<R(Parms,Parms2,Parms3)>
		{
			template<class C,template<class>class Iface, int N>
			struct inner{

				typedef R (C::*MFT)(Parms,Parms2,Parms3);

				typedef R ret_t;
				template<MFT mf>
				static std::function<R(Parms,Parms2,Parms3)> get_func(C* c){
					return [c](Parms p,Parms2 p2,Parms3 p3){
						return (c->*mf)(p,p2,p3);
					};
				}

			};
		};
		template<class R,class Parms,class Parms2,class Parms3,class Parms4>
		struct mem_fn_helper<R(Parms,Parms2,Parms3,Parms4)>
		{
			template<class C,template<class>class Iface, int N>
			struct inner{

				typedef R (C::*MFT)(Parms,Parms2,Parms3,Parms4);

				typedef R ret_t;
				template<MFT mf>
				static std::function<R(Parms,Parms2,Parms3,Parms4)> get_func(C* c){
					return [c](Parms p,Parms2 p2,Parms3 p3,Parms4 p4){
						return (c->*mf)(p,p2,p3,p4);
					};
				}
			};
		};
		template<class R,class Parms,class Parms2,class Parms3,class Parms4,class Parms5>
		struct mem_fn_helper<R(Parms,Parms2,Parms3,Parms4,Parms5)>
		{
			template<class C,template<class>class Iface, int N>
			struct inner{

				typedef R (C::*MFT)(Parms,Parms2,Parms3,Parms4,Parms5);

				typedef R ret_t;
				template<MFT mf>
				static std::function<R(Parms,Parms2,Parms3,Parms4,Parms5)> get_func(C* c){
					return [c](Parms p,Parms2 p2,Parms3 p3,Parms4 p4,Parms5 p5){
						return (c->*mf)(p,p2,p3,p4,p5);
					};
				}
			};
		};
	}


	template<template<class> class Iface,template<class> class T, int Id,class F>
	struct cross_function<Iface<implement_interface<T>>,Id,F>:public detail::cross_function_implementation<true,Iface,Id + Iface<implement_interface<T>>::base_sz,F>{
		enum{N = Id + Iface<implement_interface<T>>::base_sz};
		typedef detail::cross_function_implementation<true,Iface,Id + Iface<implement_interface<T>>::base_sz,F> cfi_t;
		cross_function(Iface<implement_interface<T>>* pi):cfi_t(
			static_cast<implement_interface<T>*>(pi)->get_portable_base()

			){}

		template<class Func>
		void operator=(Func f){
			cfi_t::set_function(*this,f);
		}
		typedef detail::mem_fn_helper<F> tm;
		template<class C, typename tm:: template inner<C,Iface,N>::MFT mf>
		void set_mem_fn (C* c){
			auto f = typename tm:: template inner<C,Iface,N>::get_func<mf>(c);
			cfi_t::set_function(*this,f);

		}
	};

	struct portable_base_holder{
		portable_base* p_;
		portable_base_holder(portable_base* p):p_(p){};

	};
	template<class b>
	struct InterfaceBase{
	public:
		enum{base_sz = 0};
		enum{sz = 0};
	};



	namespace detail{

		template<template<class> class Iface>
		class reinterpret_portable_base_t{
			portable_base* p_;
		public:
			explicit reinterpret_portable_base_t(portable_base* p):p_(p){}
			portable_base* get()const{return p_;}

		};
	}

	template<template<class> class Iface>
	detail::reinterpret_portable_base_t<Iface> reinterpret_portable_base(portable_base* p){
		return detail::reinterpret_portable_base_t<Iface>(p);
	}

	template<template <class> class Iface>
	struct use_interface:private portable_base_holder, public Iface<use_interface<Iface>>{ // Usage


		use_interface(std::nullptr_t p = nullptr):portable_base_holder(nullptr){}

		explicit use_interface(detail::reinterpret_portable_base_t<Iface> r):portable_base_holder(r.get()){}

		portable_base* get_portable_base()const{
			return this->p_;
		}

		 operator bool()const{
			return get_portable_base();
		}

		void reset_portable_base(){
			*this = nullptr;
		}


	private:
		enum{num_functions = sizeof(Iface<size_only>)/sizeof(cross_function<Iface<size_only>,0,void()>)};

		// Padding etc that makes an interface larger than a multiple of cross_function
		enum{extra = sizeof(Iface<size_only>)%sizeof(cross_function<Iface<size_only>,0,void()>)};

		// Simple checksum that takes advantage of the fact that 1+2+3+4...n = n(n+1)/2
		enum{checksum = sizeof(Iface<checksum_only>)/sizeof(cross_function<InterfaceBase<checksum_only>,0,void()>)};

		// Sanity check to make sure the total size is evenly divisible by the size of size_only cross function
		static_assert(extra==0,"Possible error in calculating number of functions");
		// Simple check to catch simple errors where the Id is misnumbered uses sum of squares
		static_assert(checksum==(num_functions * (num_functions +1)*(2*num_functions + 1 ))/6,"The Id's for a cross_function need to be ascending order from 0, you have possibly repeated a number");

	};



	template<template <class> class Iface>
	use_interface<Iface> create(const module& m,std::string func){
		typedef portable_base* (CROSS_CALL_CALLING_CONVENTION *CFun)();
		auto f = m.load_module_function<CFun>(func);
		return use_interface<Iface>(reinterpret_portable_base<Iface>(f()));


	}



	template<template<class> class Iface>
	struct implement_interface:private vtable_n<sizeof(Iface<size_only>)/sizeof(cross_function<Iface<size_only>,0,void()>)>,public Iface<implement_interface<Iface>>{ // Implementation


		implement_interface(){
			int k = sizeof(Iface<checksum_only>)/sizeof(cross_function<InterfaceBase<checksum_only>,0,void()>);

		}

		void set_runtime_parent(use_interface<Iface> parent){
			vtable_n_base* vnb = this;
			vnb->runtime_parent_ = parent.get_portable_base();
		}

		enum{num_functions = sizeof(Iface<size_only>)/sizeof(cross_function<Iface<size_only>,0,void()>)};

		// Padding etc that makes an interface larger than a multiple of cross_function
		enum{extra = sizeof(Iface<size_only>)%sizeof(cross_function<Iface<size_only>,0,void()>)};

		// Simple checksum that takes advantage of sum of squares
		// Note that on implementations where the MAX_SIZE is not able to accommodate array elements in the millions this could fail
		enum{checksum = sizeof(Iface<checksum_only>)/sizeof(cross_function<InterfaceBase<checksum_only>,0,void()>)};

		// Sanity check to make sure the total size is evenly divisible by the size of size_only cross function
		static_assert(extra==0,"Possible error in calculating number of functions");
		// Simple check to catch simple errors where the Id is misnumbered uses sum of squares
		static_assert(checksum==(num_functions * (num_functions +1)*(2*num_functions + 1 ))/6,"The Id's for a cross_function need to be ascending order from 0, you have possibly repeated a number");

		using  vtable_n<sizeof(Iface<size_only>)/sizeof(cross_function<Iface<size_only>,0,void()>)>::get_portable_base;
		const use_interface<Iface> get_use_interface(){return use_interface<Iface>(reinterpret_portable_base<Iface>(get_portable_base()));}
	};




	template<class b,template<class> class Base = InterfaceBase >
	struct define_interface:public Base<b>{

		enum{base_sz = sizeof(Base<size_only>)/sizeof(cross_function<Base<size_only>,0,void()>)};

		typedef define_interface base_t;
	};

}

#include "implementation/cross_compiler_conversions.hpp"
#include "implementation/custom_cross_function.hpp"
#pragma warning(pop)
#endif